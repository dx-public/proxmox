echo "Bootstrap Config for Proxmox"
echo "----------------------------" 

echo "Disable subscription Promox updates"
awk '// {sub(/^deb/, "#deb"); print}' /etc/apt/sources.list.d/pve-enterprise.list

echo "Get Linux codename"
codename=$(awk '/CODENAME/' /etc/*-release | awk -F'=' '{$1=""; print}' )
echo "codename is : $codename"

echo "Get Proxmox updates"
echo "deb http://download.proxmox.com/debian/pve $codename pve-no-subscription" > /etc/apt/sources.list.d/pve-no-subscription.list

echo "Secure sources"
echo "deb https://ftp.debian.org/debian $codename main contrib" > /etc/apt/sources.list
echo "deb https://ftp.debian.org/debian ${codename}-updates main contrib" >> /etc/apt/sources.list
echo "deb https://security.debian.org ${codename}-security main contrib" >> /etc/apt/sources.list

echo "Get microcode repo"
echo "deb https://deb.debian.org/debian/ unstable non-free-firmware" > /etc/apt/sources.list.d/debian-unstable.list
cat > /etc/apt/preferences.d/unstable-repo << EOF
# lower the priority of all packages in the unstable repository
Package: *
Pin: release o=Debian,a=unstable
Pin-Priority: 10

# allow upgrading microcode from the unstable repository
Package: intel-microcode
Pin: release o=Debian,a=unstable
Pin-Priority: 500
# This line should be a blank line or comment
Package: amd64-microcode
Pin: release o=Debian,a=unstable
Pin-Priority: 500
EOF

echo "Changing Static Crontab Values"
minh=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/random)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
mind=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/random)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
minw=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/random)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
minm=`awk -v min=19 -v max=53 -v seed="$(od -An -N4 -tu4 /dev/random)" 'BEGIN{srand(seed+0); print int(min+rand()*(max-min+1))}'`
sed -e "s/17/$minh/g" -e "s/25/$mind/g" -e "s/47/$minw/g" -e "s/52/$minm/g" /etc/crontab > /tmp/crontab && mv /tmp/crontab /etc/crontab


echo "Running update from repos"
date -s "$(curl --retry 5 --retry-max-time 40 --max-time 10 --connect-timeout 99.9 -sI https://nist.time.gov/timezone.cgi?UTC/s/0 | awk -F': ' '/Date: / {print $2}')"
apt update && apt dist-upgrade -y
echo "#!/bin/bash" > /etc/cron.daily/update-packages
echo "apt update && apt dist-upgrade -y" >> /etc/cron.daily/update-packages
echo "if [ -e /var/run/reboot-required ]; then /sbin/shutdown -r 15 'reboot in fifteen minutes'; fi" >> /etc/cron.daily/update-packages
chmod +x /etc/cron.daily/update-packages

echo "Get processor for microcode"
journalctl -k --grep="microcode updated early to"
modelname=$(cat /proc/cpuinfo | awk '/model name/' | awk -F '\n' '{print $1;exit}' )
isintel=$(echo $modelname | awk '/Intel/')
if [ ! -z "$isintel" ]; then echo "You have an Intel processor" >&2; apt install intel-microcode; fi
isamd=$(echo $modelname | awk '/AMD/')
if [ ! -z "$isamd" ]; then echo "You have an AMD processor" >&2; apt install amd64-microcode; fi
journalctl -k --grep="microcode updated early to"

echo "Fix Networking stack"
echo "2" > /proc/sys/net/ipv4/conf/all/arp_ignore
echo "net.ipv4.conf.all.arp_ignore=2" > /etc/sysctl.d/99-arp_ignore.conf

echo "Install script for console access to VMs on host"
#apt-get install -y openbox firefox-esr xinit xdotool
mkdir -p /root/proxmox-scripts
cd /root/proxmox-scripts
wget https://gitlab.com/dx-public/proxmox/-/raw/main/install.sh -O /root/proxmox-scripts/install-prox-kiox.sh && chmod +x /root/proxmox-scripts/install-prox-kiox.sh
echo "to install proxmox console access run: /root/proxmox-scripts/install-prox-kiox.sh"

